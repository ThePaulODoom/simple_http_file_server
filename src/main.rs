use std::fs;
use std::env;
use std::net::TcpListener;
use std::io::Write;
use std::process;

fn main() {
    let args: Vec<String> = env::args().collect();
    let arglen = args.len();

    if arglen != 2 {
        eprintln!("usage: {} [file]", &args[0]);
        process::exit(1);
    }

    let contents = fs::read_to_string(&args[1]).expect("Failed reading file");

    let listener = TcpListener::bind("0.0.0.0:7878").unwrap();

    let length = contents.len();

    for stream in listener.incoming() {
        let mut stream = stream.unwrap();
        let response = format!("HTTP/1.1 200 OK\r\nContent-Length: {length}\r\n\r\n{contents}");
        stream.write_all(response.as_bytes()).unwrap();

        println!("Connection established");
    }
}
