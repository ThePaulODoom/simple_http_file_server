simple_http_file_server
-------
This is a simple application that binds to port 7878 and serves a single file over HTTP

``usage: ./simple_http_file_server [file]``
